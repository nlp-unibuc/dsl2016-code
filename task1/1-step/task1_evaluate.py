import os
import sys
import codecs
import scipy
import pickle
import logging

import numpy as np
import pandas as pd
import csv	

from sklearn.model_selection import StratifiedKFold
from sklearn import metrics
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.externals import joblib



logging.basicConfig(format = u'[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.NOTSET)

data_train = pd.read_csv("../data/input/task1-train.txt", '\t', quoting=csv.QUOTE_NONE)
data_test = pd.read_csv("../data/input/task1-dev.txt", '\t', quoting=csv.QUOTE_NONE)

lab2num = dict()
num2lab = dict()
for i,l in enumerate(set(data_train['sublabels'] + data_test['sublabels'])):
    lab2num[l] = i
    num2lab[i] = l

logging.info('Applying tf-idf')
    
tfv = TfidfVectorizer(min_df=3,  max_features=None, 
    strip_accents='unicode', analyzer='word', token_pattern=r'\w{1,}',
    ngram_range=(1, 2), use_idf=1,smooth_idf=1,sublinear_tf=1)
   
traindata = data_train['sentence'].values
y = data_train['sublabels'].replace(lab2num).values
    
testdata = data_test['sentence'].values
y_test = data_test['sublabels'].replace(lab2num).values

X_all = list(traindata) + list(testdata)
lentrain = len(traindata)

tfv.fit(X_all)
X_all = tfv.transform(X_all)
X = X_all[:lentrain]
X_test = X_all[lentrain:]

model = LogisticRegression(penalty='l2', dual=True, tol=0.0001, 
                     C=1, fit_intercept=True, intercept_scaling=1.0, 
                     class_weight=None, random_state=None)
    
model.fit(X,y)
result = model.predict(X_test)

score = metrics.accuracy_score(y_test, result)
print score

'''
$ python task1_evaluate.py 
[LINE:32]# INFO     [2016-09-18 17:15:14,968]  Applying tf-idf
0.844125
'''

