import os
import sys
import codecs
import scipy
import pickle
import logging

import numpy as np
import pandas as pd
import csv	

from sklearn.model_selection import StratifiedKFold
from sklearn import metrics
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.externals import joblib



logging.basicConfig(format = u'[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.NOTSET)


lang_list = ["es", "fr", "idmy", "pt", "srb"]
for lang in lang_list:
    data_train = pd.read_csv("../data/input/split/" + lang + "/task1-train-" + lang + ".txt", '\t', quoting=csv.QUOTE_NONE)
    data_test = pd.read_csv("../data/input/split/" + lang + "/task1-dev-" + lang + ".txt", '\t', quoting=csv.QUOTE_NONE)

    print("Language: " + lang)

    lab2num = dict()
    num2lab = dict()
    for i,l in enumerate(set(data_train['labels'] + data_test['labels'])):
        lab2num[l] = i
        num2lab[i] = l

    logging.info('Applying tf-idf')
    
    tfv = TfidfVectorizer(min_df=3,  max_features=None, 
        strip_accents='unicode', analyzer='word', token_pattern=r'\w{1,}',
        ngram_range=(1, 2), use_idf=1,smooth_idf=1,sublinear_tf=1)
   
    traindata = data_train['sentence'].values
    y = data_train['labels'].replace(lab2num).values
    
    testdata = data_test['sentence'].values
    y_test = data_test['labels'].replace(lab2num).values

    X_all = list(traindata) + list(testdata)
    lentrain = len(traindata)

    tfv.fit(X_all)
    X_all = tfv.transform(X_all)
    X = X_all[:lentrain]
    X_test = X_all[lentrain:]

    model = LogisticRegression(penalty='l2', dual=True, tol=0.0001, 
                     C=1, fit_intercept=True, intercept_scaling=1.0, 
                     class_weight=None, random_state=None)
    
    model.fit(X,y)
    result = model.predict(X_test)

    score = metrics.accuracy_score(y_test, result)
    print score

'''
$ python task1_evaluate_variety.py 
Language: es
[LINE:37]# INFO     [2016-09-18 19:17:35,377]  Applying tf-idf
0.751
Language: fr
[LINE:37]# INFO     [2016-09-18 19:17:59,397]  Applying tf-idf
0.894
Language: idmy
[LINE:37]# INFO     [2016-09-18 19:18:10,812]  Applying tf-idf
0.982
Language: pt
[LINE:37]# INFO     [2016-09-18 19:18:20,531]  Applying tf-idf
0.92075
Language: srb
[LINE:37]# INFO     [2016-09-18 19:18:34,287]  Applying tf-idf
0.784833333333
'''

