import os
import sys
import codecs
import scipy
import pickle
import logging

import numpy as np
import pandas as pd
import csv	

from sklearn.model_selection import StratifiedKFold
from sklearn import metrics
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.externals import joblib



logging.basicConfig(format = u'[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.NOTSET)

lang_list = ["es", "fr", "pt", "idmy", "srb"]
dataset_list = ["A", "B1", "B2"]

for lang in lang_list:
    for dataset in dataset_list:

        try:
            data_train = pd.read_csv("../data/input/split/" + lang + "/task1-train-dev-" + lang + ".txt", '\t', quoting=csv.QUOTE_NONE)
            with open("../data/output/2-steps/group/" + dataset + "_predictions_" + lang + ".txt") as f:
                testdata = f.readlines()

            print("Language: " + lang)
            print("Dataset: " + dataset)

	    lab2num = dict()
            num2lab = dict()
 	    for i,l in enumerate(set(data_train["labels"])):
	        lab2num[l] = i
	        num2lab[i] = l

	    logging.info('Applying tf-idf')
	    
	    tfv = TfidfVectorizer(min_df=3,  max_features=None, 
	        strip_accents='unicode', analyzer='word', token_pattern=r'\w{1,}',
	        ngram_range=(1, 2), use_idf=1,smooth_idf=1,sublinear_tf=1)
	   
	    traindata = data_train['sentence'].values
	    y = data_train['labels'].replace(lab2num).values

	    X_all = list(traindata) + list(testdata)
	    lentrain = len(traindata)

	    tfv.fit(X_all)
	    X_all = tfv.transform(X_all)
	    X = X_all[:lentrain]
	    X_test = X_all[lentrain:]

  	    model = LogisticRegression(penalty='l2', dual=True, tol=0.0001, 
		                 C=1, fit_intercept=True, intercept_scaling=1.0, 
		             class_weight=None, random_state=None)
	    
	    model.fit(X,y)
	    result = model.predict(X_test)

	    with open ("../data/output/2-steps/variety/" + dataset + "_predictions_" + lang + ".txt", "w") as predictions_out:
	        for sent, num in zip(testdata, result):
   	            predictions_out.write(sent.rstrip() + "\t" + num2lab[num] + "\n")
        except:
            print "error for " + lang + " and " + dataset
